#!/bin/bash
for i in /tmp/*.pid; do
  tp=$(cat $i)
  if [ $(ps ${tp}|grep ${tp}|wc -l) -eq 0 ]; then
    rm $i
	nohup /home/pi/gps/gps_raw5.sh $(basename $i .pid) &>/dev/null &
  fi
done
