#!/bin/bash
TTY=$1
echo ${TTY}
if [ ! -f /tmp/${TTY}.pid ]; then
echo ${TTY}
id=
#	echo $$ > /tmp/${TTY}.pid
	uid=$(echo $(udevadm info --name=/dev/${TTY}|sed -n '/MAJOR/{s/[A-Z: =]*//;p}')*256+$(udevadm info --name=/dev/${TTY}|sed -n '/MINOR/{s/[A-Z: =]*//;p}')|bc)
	id=$(date +%N|cut -b 3-6)
	exfile=$(date +%j%H%M)
	while IFS=$ read -r pre gline; do
		echo $gline
		if [ ! -z "$gline" ]; then
			gline=$(echo $gline|cut -d\* -f1)
			echo $gline
			telegram=$(echo $gline|cut -c3-5)
			echo $telegram
		fi
	done </dev/${TTY}
fi
