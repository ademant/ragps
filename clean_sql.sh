#!/bin/sh
mysql -u gps -pgps -D gps -e "
delete from gps_gga where sats is null or sats>15;
delete from gps_gsv where ceil(prn)>prn or elevation is null or azimuth is null;
delete from gps_rmc where receiver is null;
"
