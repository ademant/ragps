txt2bl<-function(indata){
	instr<-strsplit(indata,"\\.")
	ins<-data.frame(a=unlist(lapply(1:length(instr),function(i){return(unlist(instr[[i]][1]))})),b=unlist(lapply(1:length(instr),function(i){return(unlist(instr[[i]][2]))})),stringsAsFactors=FALSE)
#	ins<-matrix(unlist(strsplit(indata,"\\.")),ncol=2,byrow=TRUE)
	if(sum(is.na(ins$a),na.rm=TRUE)>0)ins[is.na(ins$a),"a"]<-"000"
	if(sum(is.na(ins$b),na.rm=TRUE)>0)ins[is.na(ins$b),"b"]<-"000"
	if(sum(nchar(ins$a)<2,na.rm=TRUE)>0)ins[nchar(ins$a)<2,"a"]<-"000"
	if(sum(nchar(ins$b)<2,na.rm=TRUE)>0)ins[nchar(ins$b)<2,"b"]<-"000"
	if(sum(nchar(ins$b)>5,na.rm=TRUE)>0)ins[nchar(ins$b)>5,"b"]<-unlist(lapply(ins[nchar(ins$b)>5,"b"],function(i){return(substr(i,1,5))}))
	insec<-as.integer(paste(ins$b,sapply(6-nchar(ins$b),function(i){return(paste0(rep("0",i),collapse=""))}),sep=""))
	inmin<-as.integer(sapply(ins$a,function(i){return(substr(i,nchar(i)-1,nchar(i)))}))+
			60*as.integer(sapply(ins$a,function(i){return(substr(i,1,nchar(i)-2))}))
	out<-inmin*1e6+insec
	return(out)
}
dbCatchWriteTable<-function(db,table,indata,nfr=500){
	ncalls<-floor(nrow(indata)/nfr)
	rowi<-1:nrow(indata)
	for(i in 0:ncalls){
		win<-i*nfr+1:nfr
		win<-win[win%in%rowi]
		dbWriteTable(db,table,indata[win,],append=TRUE,row.names=FALSE)
	}
}
