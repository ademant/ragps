#!/bin/bash
i=0
rmc=
baud=
#if [ -f /tmp/$1 ]; then rm /tmp/$1; fi
while [ -z ${rmc} ]; do
	read line </dev/$1
#	line=$(head /dev/$1 -n1)
#	if [ -z ${line} ]; then rmc=3; fi
	if [ $(echo ${line}|grep \$G|wc -l) -gt 0 ]; then 
		rmc=1
		baud=$(stty -F /dev/$1 speed)
		echo $(stty -F /dev/$1 speed) >/tmp/$1
	fi
	i=$(echo $i + 1|bc)
	if [ $i -gt 10 ]; then rmc=2; fi
done

echo $rmc
exit $rmc

#test=$(dd if=/dev/$1 bs=1 count=1000|grep RMC|wc -l)
#if [ $test -gt 0 ]; then
#	exit 14
#else
#	exit 0
#fi

