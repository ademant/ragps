#!/bin/bash
dbe=$(sqlite3 /home/pi/gps.sqlite "select max(epoch) from GPS_TTY;")
if [ -z "$dbe" ]; then
echo Anlegen TTY-Tabelle
sqlite3 /home/pi/gps.sqlite "create table GPS_TTY (EPOCH REAL,TTY TEXT,IDPRODUCT TEXT,IDVENDOR TEST,BUS TEXT);"
fi
sudo hcitool cc 00:0B:0D:88:95:E9
sudo rfcomm bind /dev/rcomm0 00:0B:0D:88:95:E9
echo $1
	idp=$(udevadm info -a /dev/rfcomm0|grep idProduct|head -n1|cut -d= -f3|sed -e 's/"//g')
	idv=$(udevadm info -a /dev/rfcomm0|grep idVendor|head -n1|cut -d= -f3|sed -e 's/"//g')
	echo $(date +%s),${TTY},${idp},${idv},$i
	sqlite3 /home/pi/gps.sqlite "insert into GPS_TTY (EPOCH,TTY,IDPRODUCT,IDVENDOR,BUS) values ($(date +%s),'${TTY}','${idp}','${idv}','$i');"
	nohup /home/pi/gps_raw2.sh rfcomm0 &

