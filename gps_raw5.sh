#!/bin/bash
export TZ=UTC
TTY=$1
STTY=$(echo $1|sed -e 's/tty//g')
if [ ! -f /tmp/${TTY}.pid ]; then
	id=
	echo $$ > /tmp/${TTY}.pid
	mac=$(printf "%d\n" 0x$(cat $(find /sys/class/net -name "en*" |head -n1)/address|tr -d ':'))
	uid=$(echo $(udevadm info --name=/dev/${TTY}|sed -n '/MAJOR/{s/[A-Z: =]*//;p}')*256+$(udevadm info --name=/dev/${TTY}|sed -n '/MINOR/{s/[A-Z: =]*//;p}')|bc)
	vendor=$(udevadm info --name=/dev/${TTY}|sed -n '/VENDOR=/{s/[A-Z:_ =]*//;p}')
	serial=$(udevadm info --name=/dev/${TTY}|sed -n '/SERIAL=/{s/[A-Z:_ =]*//;p}')
	model=$(udevadm info --name=/dev/${TTY}|sed -n '/MODEL=/{s/[A-Z:_ =]*//;p}')
	path=$(udevadm info --name=/dev/${TTY}|sed -n '/DEVPATH=/{s/[A-Z:_ =]*//;p}')
	uvs="${mac}§${uid}§${vendor}§${serial}§${model}§${path}"
#	cat /dev/${TTY}|unbuffer -p tr -dc "[:print:]\n"|ts %.s,${STTY},${uid},|unbuffer -p sed -e 's/*..//g'|/home/pi/gps/gps_pre_sql.awk| $(mysql -u gps -D gps -pgps --batch )
#	cat /dev/${TTY}|unbuffer -p tr -dc "[:print:]\n"|ts %.s,${STTY},${uid},|unbuffer -p sed -e 's/*..//g'|/home/pi/gps/gps_pre.awk|split -a12 -l1000 - /tmp/gps_${uid}. --filter='gzip -9 > $FILE.gz'
#	tr -dc "[:print:]\n" < /dev/${TTY} |ts %.s,${uid},|unbuffer -p sed -e 's/*..//g'|/home/pi/gps/gps_pre.awk|split -a12 -l1000 - /tmp/gps_${uid}. --filter='bzip2 -9 > $FILE.bz2'
	tr -dc "[:print:]\n" < /dev/${TTY} |ts %.s,${mac},${uvs},|unbuffer -p sed -e 's/*..//g'|/home/pi/gps/gps_pre.awk|split -a12 -l10000 - /tmp/gps_${uid}. --filter='xz -9 > $FILE.xz'
fi
