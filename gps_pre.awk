#!/usr/bin/awk -f
BEGIN {FS =","
	OFS=","}
/RMC/ {
	gsub(/\./,"",$1)
#	gsub(/ \$/,"",$5)
	split($4,tel,"$")
	gsub(/^0/,"",$11)
	split($5,zeit,".")
	split($7,w,".")
	for(i=length(w[2]);i<6;i++)w[2]=w[2] "0";
	deg=substr(w[1],1,length(w[1])-2)*60+substr(w[1],length(w[1])-1,2)
	ndeg=""
	if($8=="S")ndeg="-"
	split($9,l,".")
	for(i=length(l[2]);i<6;i++)l[2]=l[2] "0";
	ldeg=substr(l[1],1,length(l[1])-2)*60+substr(l[1],length(l[1])-1,2)
	nldeg=""
	if($10=="W")nldeg="-"
    utc=mktime("20"substr($13,5,2)" "substr($13,3,2)" "substr($13,1,2)" "substr($5,1,2)" "substr($5,3,2)" "substr($5,5,2))
    if($12=="")$12=0
#	print "RMC",$1,$3,substr($4,2,1),zeit[1]+0,ndeg deg w[2],nldeg ldeg l[2],$11*1000,$12 
	print "RMC",$1,$2,$3,substr(tel[2],2,1),zeit[1]+0,utc,ndeg deg w[2],nldeg ldeg l[2],$11*1000,$12+0 
	}
/GGA/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4);gsub(/^0/,"",$11);split($5,ts,".");gsub(/^0/,"",$8)
	print "GGA",$1,$2,$3,substr($4,2,1),$11,$12*1000,$13*1000,$15*1000 
	}
/GSA/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4)
	print "GSA",$1,$2,$3,substr($4,2,1),$19*1000,$20*1000,$21*1000
	}
/GSV/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4)
	for(i=1;i<=4;i++){
		if(i*4+7<NF){
			bp=1
			if(length($(i*4+7))==0){$(i*4+7)=0}
			if($(i*4+7)=="\r"){$(i*4+7)=0}
			print "GSV",$1,$2,$3,substr($4,2,1),$(4*i+4)+0,$(4*i+5)+0,$(4*i+6)+0,$(4*i+7)+0
			}
	}
	}
/GST/ {
	gsub(/\./,"",$1)
	split($5,zeit,".")
	gsub(/ \$/,"",$4)
	print "GST",$1,$2,$3,substr($4,2,1),$6+0,$7+0,$8+0,$9+0,$10*1000,$11*1000,$12*1000
	}
