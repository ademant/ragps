#!/bin/sh
gpsl=
pid=$$
fl=""
for i in $(find /tmp -name "gps_*.*"|cut -d/ -f3|cut -d\. -f1|sort -u); do
  fl="${fl} $(find /tmp -name "$i.*"|sort|head -n-1)"
done
#echo $fl|xargs -P4 -n1 bunzip2 -k
for i in $fl; do
  unxz $i
  j=$(basename $i .xz)
  cat /tmp/$j|grep RMC|grep -v ,,|cut -d, -f2- |xz -9 >> /tmp/${pid}.rmc.xz
  cat /tmp/$j|grep GSV|cut -d, -f2- |xz -9 >> /tmp/${pid}.gsv.xz
  cat /tmp/$j|grep GSA|cut -d, -f2- |xz -9 >> /tmp/${pid}.gsa.xz
  cat /tmp/$j|grep GST|cut -d, -f2- |xz -9 >> /tmp/${pid}.gst.xz
  cat /tmp/$j|grep GGA|cut -d, -f2- |xz -9 >> /tmp/${pid}.gga.xz
  rm /tmp/$j  
done
unxz /tmp/${pid}.rmc.xz
mysql -h 10.8.0.14 -u gps -pgps -D gps -e "load data local infile '/tmp/${pid}.rmc' into table gpsrmc fields terminated by ',' enclosed by ';' (epoch,mac,uid,system,fix,utc,breite,laenge,velocity,course);"
rm /tmp/${pid}.rmc
unxz /tmp/${pid}.gsv.xz
mysql -h 10.8.0.14 -u gps -pgps -D gps -e "load data local infile '/tmp/${pid}.gsv' into table gpsgsv fields terminated by ',' enclosed by ';' (epoch,mac,uid,system,prn,elevation,azimuth,snr);"
rm /tmp/${pid}.gsv
unxz /tmp/${pid}.gsa.xz
mysql -h 10.8.0.14 -u gps -pgps -D gps -e "load data local infile '/tmp/${pid}.gsa' into table gpsgsa fields terminated by ',' enclosed by ';' (epoch,mac,uid,system,pdop,hdop,vdop);"
rm /tmp/${pid}.gsa
unxz /tmp/${pid}.gst.xz
mysql -h 10.8.0.14 -u gps -pgps -D gps -e "load data local infile '/tmp/${pid}.gst' into table gpsgst fields terminated by ',' enclosed by ';' (epoch,mac,uid,system,rms,errmaj,errmin,error,sig_lat,sig_lon,sig_height);"
rm /tmp/${pid}.gst
unxz /tmp/${pid}.gga.xz
mysql -h 10.8.0.14 -u gps -pgps -D gps -e "load data local infile '/tmp/${pid}.gga' into table gpsgga fields terminated by ',' enclosed by ';' (epoch,mac,uid,system,sats,hdop,hoehe,geoid);"
rm /tmp/${pid}.gga

#rm /tmp/${pid}.*
