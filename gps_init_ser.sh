#!/bin/bash
sqlite_com="sqlite3 /home/pi/gps.sqlite"
mysql_com="mysql -u gps -pgps -D gps -e"
TTY=$1
rm /tmp/$1
baud=16384
for i in 4800 9600 19200 38400 57600 115200
do
  echo $i
  stty $i -F /dev/${TTY}
#  tds=$(timeout 10s $(test=$(dd if=/dev/$1 bs=1 count=10000|grep RMC|wc -l);if [ $test -gt 0 ]; then exit 1; else exit 0; fi))
  timeout 10s /home/pi/gps/check_baud.sh ${TTY}
#  echo $?
  if [ $? -eq "1" ]; then 
	echo Test
	  baud=$i
  fi
done
#if [ -f /tmp/$1 ]; then
#	baud=$(cat /tmp/$1)
#fi

#exit 1
echo $baud
#exit 1

if [ $baud -ne 16384 ]; then
	echo ${TTY}
	echo $baud
	stty $baud -F /dev/${TTY}
	dbe=$(sqlite3 /home/pi/gps.sqlite "select max(utc) from GPS_TTY;")
	if [ -z "$dbe" ]; then
		echo Anlegen TTY-Tabelle
		sqlite3 /home/pi/gps.sqlite "create table GPS_TTY (uid integer,tty text,utc integer,idvendor text,idmodel text,baud integer);"
	fi
#    TTY=$(echo $i|cut -d/ -f3)
    uid=$(echo $(udevadm info --name=/dev/${TTY}|sed -n '/MAJOR/{s/[A-Z: =]*//;p}')*256+$(udevadm info --name=/dev/${TTY}|sed -n '/MINOR/{s/[A-Z: =]*//;p}')|bc)
	idp=$(udevadm info -a -q property --export -n /dev/${TTY}|grep ID_MODEL_ID|cut -d= -f2|sed -e "s/'//g")
	idv=$(udevadm info -a -q property --export -n /dev/${TTY}|grep ID_VENDOR_ID|cut -d= -f2|sed -e "s/'//g")
   idpath=$(udevadm info -a -q property --export -n ${TTY}|grep ID_PATH=|cut -d= -f2|sed -e "s/'//g")
   echo $(date +%s),${TTY},${idp},${idv},$i
	${sqlite_com} "insert into GPS_TTY (uid,tty,utc,idvendor,idmodel,baud,path) values (${uid},'${TTY}',$(date +%s),'${idp}','${idv}',$(stty -F /dev/${TTY} speed),'${idpath}');"
	${mysql_com} "insert into gpstty (uid,tty,utc,idvendor,idmodel,baud,path) values (${uid},'${TTY}',$(date +%s),'${idp}','${idv}',$(stty -F /dev/${TTY} speed),'${idpath}');"
	nohup /home/pi/gps/gps_raw5.sh ${TTY} &
fi

