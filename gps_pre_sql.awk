#!/usr/bin/awk -f
BEGIN {FS =","
	OFS=""}
/RMC/ {
	gsub(/\./,"",$1)
	gsub(/ \$/,"",$4)
	gsub(/^0/,"",$11)
	split($5,zeit,".")
	split($7,w,".")
	for(i=length(w[2]);i<6;i++)w[2]=w[2] "0";
	deg=substr(w[1],1,length(w[1])-2)*60+substr(w[1],length(w[1])-1,2)
	ndeg=""
	if($8=="S")ndeg="-"
	split($9,l,".")
	for(i=length(l[2]);i<6;i++)l[2]=l[2] "0";
	ldeg=substr(l[1],1,length(l[1])-2)*60+substr(l[1],length(l[1])-1,2)
	nldeg=""
	if($10=="W")nldeg="-"
    utc=mktime("20"substr($13,5,2)" "substr($13,3,2)" "substr($13,1,2)" "substr($5,1,2)" "substr($5,3,2)" "substr($5,5,2))
    if($12=="")$12=0
	print "insert into gpsrmc (epoch,uid,system,fix,breite,laenge,velocity,course) values (",$1,",",$3,",'",substr($4,2,1),"',",zeit[1]+0,",",ndeg deg w[2],",",nldeg ldeg l[2],",",$11*1000,",",$12,");" 
	}
/GGA/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4);gsub(/^0/,"",$11);split($5,ts,".");gsub(/^0/,"",$8);print "insert into gpsgga (epoch,uid,system,sats,hdop,hoehe,geoid) values ("$1,",",$3,",'",substr($4,2,1),"',",$11,",",$12*1000,",",$13*1000,",",$15*1000,");" }
/GSA/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4);print "insert into gpsgsa (epoch,uid,system,pdop,hdop,vdop) values ("$1,",",$3,",'",substr($4,2,1),"',",$19*1000,",",$20*1000,",",$21*1000,");"}
/GSV/ {gsub(/\./,"",$1);gsub(/ \$/,"",$4)
    bp=0
	for(i=1;i<=4;i++){
		if(i*4+7<NF){
			bp=1
			if(i==1){print "insert into gpsgsv (epoch,uid,system,prn,elevation,azimuth,snr) values "}
			if(i>1){print ","}
			if(length($(i*4+7))==0){$(i*4+7)=0}
			if($(i*4+7)=="\r"){$(i*4+7)=0}
#	    if((i*4+4)<NR){
#			if($(i*4+7)==""){$(i*4+7)=0}
			print "(",$1,",",$3,",'",substr($4,2,1),"',",$(4*i+4)+0,",",$(4*i+5)+0,",",$(4*i+6)+0,",",$(4*i+7)+0,")"
			}
	}
	if(bp==1){print ";"}
	}
#	}
/GST/ {
	gsub(/\./,"",$1)
	split($5,zeit,".")
	gsub(/ \$/,"",$4)
	print "insert into gpsgst (epoch,uid,system,rms,errmaj,errmin,error,sig_lat,sig_lon,sig_height) values (",$1,",",$3,",'",substr($4,2,1),"',",$6+0,",",$7+0,",",$8+0,",",$9+0,",",$10*1000,",",$11*1000,",",$12*1000,");" > "/tmp/gst"$3"."$1
	}
