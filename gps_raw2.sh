#!/bin/bash
TTY=$1
echo ${TTY}
id=$(date +%N|cut -b 3-6)

grep G /dev/${TTY}|while read line
do
  case $line in
    *RMC*)
		id=$(date +%N|cut -b 3-6)
		echo ${TTY},$(expr $(date +%s) - 1514764800).$(date +%N|cut -b -5),$id,$line |sed -e 's/tty//g;s/\.[0]*,/,/g;s/*.*//g;s/[,]*$//g;s/,0*/,/g' >> /tmp/gps_$(date +%j%H%M)
    ;;
    *TXT*)
    ;;
    *unknown*)
    ;;
    *VTG*)
    ;;
    *PUBX*)
    ;;
    *G*)
		echo ${TTY},$(expr $(date +%s) - 1514764800).$(date +%N|cut -b -5),$id,$line |sed -e 's/tty//g;s/\.[0]*,/,/g;s/*.*//g;s/[,]*$//g;s/,0*/,/g' >> /tmp/gps_$(date +%j%H%M)
	;;
  esac
done

