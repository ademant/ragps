import socket,threading,json,time
from queue import Queue


def gps_insert(q):
	gpsdata=[]
	while True:
		if q.empty():
			time.sleep(0.1)
		else:
			try:
				indata=q.get()
				if indata is not None:
					q.task_done()
			except Exception as e:
				print("Error during queuing")
				print(e)
			else:
				if indata is not None:
					print(indata)

class Broker():
	def __init__(self,q):
		self.sock=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
		self.sock.bind(('127.0.0.1',24045))
		self.queue=q
	def listen_clients(self):
		while True:
			msg,client=self.sock.recvfrom(1024)
			print(msg)

if __name__ == "__main__":
    
    q=Queue(maxsize=0)
    
    sql_worker=threading.Thread(target=gps_insert,args=(q,))
    sql_worker.setDaemon(True)
    sql_worker.start()
    
    # Port 0 means to select an arbitrary unused port
    
    b=Broker(q)
    b.listen_clients()
