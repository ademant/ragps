#!/bin/bash
echo $1 >>/home/pi/gps.log
date +%s >>/home/pi/gps.log
dbe=$(sqlite3 /home/pi/gps.sqlite "select max(epoch) from GPS_TTY;")
if [ -z "$dbe" ]; then
echo Anlegen TTY-Tabelle
sqlite3 /home/pi/gps.sqlite "create table GPS_TTY (EPOCH REAL,TTY TEXT,IDPRODUCT TEXT,IDVENDOR TEST,BUS TEXT);"
fi
uda=$(udevadm info -a /dev/$1)
echo $1
if [ $(echo $uda|grep GNSS|wc -l) -gt 0 ]; then
	idp=$(echo $uda|grep idProduct|head -n1|cut -d= -f3|sed -e 's/"//g')
	idv=$(echo $uda|grep idVendor|head -n1|cut -d= -f3|sed -e 's/"//g')
	echo $(date +%s),${TTY},${idp},${idv},$i
	sqlite3 /home/pi/gps.sqlite "insert into GPS_TTY (EPOCH,TTY,IDPRODUCT,IDVENDOR,BUS) values ($(date +%s),'${TTY}','${idp}','${idv}','$i');"
	nohup /home/pi/gps/gps_raw3.sh $1 &
fi

echo $1,$(date +%s) >>/home/pi/gps.log
