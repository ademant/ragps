#!/bin/bash
TTY=$1
echo ${TTY}
ntty=$(echo $TTY|sed -e 's/tty//g')
#id=$(date +%N|cut -b 3-6)
id="NOFIX"

grep G /dev/${TTY}|while read line
do
  if [ "$line" == "*RMC*" ]; then
		if [ "$line" == "*,,,,*" ]; then
		  id="NOFIX"
		else
		  id=$(date +%N|cut -b 3-6)
		  echo ${ntty},$(date +%s).$(date +%N|cut -b -3),$id,$line |sed -e 's/,[NWESAV]/,/g;s/\.[0]*,/,/g;s/*.*//g;s/,0*/,/g;s/[,]*$//g' >> /tmp/gps_$(date +%j%H%M)
		fi
  fi
  if [ "$id" != "NOFIX" ]; then
	  case $line in
		*TXT*,*unknown*,*VTG*,*PUBX*)
		;;
		*GSV*)
				echo ${ntty},$(date +%s).$(date +%N|cut -b -3),$id,$line |sed -e 's/*.*//g;s/,0*/,/g' >> /tmp/gps_$(date +%j%H%M)
		;;
		*GSA*)
				echo ${ntty},$(date +%s).$(date +%N|cut -b -3),$id,$line |sed -e 's/*.*//g;s/,0*/,/g;s/\.[0]*,/,/g' >> /tmp/gps_$(date +%j%H%M)
		;;
		*GGA*)
				echo ${ntty},$(date +%s).$(date +%N|cut -b -3),$id,$line |sed -e 's/,[MNESW]/,/g;s/*.*//g;s/,0*/,/g;s/\.[0]*,/,/g' >> /tmp/gps_$(date +%j%H%M)
		;;
	  esac
  fi
done

