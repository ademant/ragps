#!/bin/bash
export TZ=UTC
TTY=$1
STTY=$(echo $1|sed -e 's/tty//g')
if [ ! -f /tmp/${TTY}.pid ]; then
	id=
	echo $$ > /tmp/${TTY}.pid
	mac=$(printf "%d\n" 0x$(cat $(find /sys/class/net -name "en*" |head -n1)/address|tr -d ':'))
	uid=$(echo $(udevadm info --name=/dev/${TTY}|sed -n '/MAJOR/{s/[A-Z: =]*//;p}')*256+$(udevadm info --name=/dev/${TTY}|sed -n '/MINOR/{s/[A-Z: =]*//;p}')|bc)
	vendor=$(udevadm info --name=/dev/${TTY}|sed -n '/VENDOR=/{s/[A-Z:_ =]*//;p}')
	serial=$(udevadm info --name=/dev/${TTY}|sed -n '/SERIAL=/{s/[A-Z:_ =]*//;p}')
	model=$(udevadm info --name=/dev/${TTY}|sed -n '/MODEL=/{s/[A-Z:_ =]*//;p}')
	path=$(udevadm info --name=/dev/${TTY}|sed -n '/DEVPATH=/{s/[A-Z:_ =]*//;p}')
	uvs="${mac}§${uid}§${vendor}§${serial}§${model}§${path}"
#	tr -dc "[:print:]\n" < /dev/${TTY} |ts %.s,${mac},${uvs},|unbuffer -p sed -e 's/*..//g'|/home/pi/ragps/gps_pre.awk|nc -u localhost 24040
	tr -dc "[:print:]\n" < /dev/${TTY} |ts %.s,${mac},${uvs},|unbuffer -p sed -e 's/*..//g'|nc -u localhost 24040

fi
